﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace AlgorithmList
{
    class CircularLinkedList<T> : IEnumerable<T>, IAlgorithm<T> where T : IComparable<T>
    {
        private Node<T> FirstNode;
        private Node<T> LastNode;
        public int CountNode { get; set; }
        public void Add(T data)
        {
            Node<T> node = new Node<T>(data);
            if (FirstNode == null)
            {
                FirstNode = node;
                LastNode = node;
                LastNode.Next = FirstNode;
            }
            else
            {
                node.Next = FirstNode;
                LastNode.Next = node;
                LastNode = node;
            }
            CountNode++;
        }
        public bool Remove(T data)
        {
            Node<T> current = FirstNode;
            Node<T> previous = null;

            if (IsEmpty) return false;
            do
            {
                if (current.Data.Equals(data))
                {
                    if (previous != null)
                    {
                        previous.Next = current.Next;
                        if (current == LastNode)
                            LastNode = previous;
                    }
                    else
                    {
                        if (CountNode == 1)
                        {
                            FirstNode = LastNode = null;
                        }
                        else
                        {
                            FirstNode = current.Next;
                            LastNode.Next = current.Next;
                        }
                    }
                    CountNode--;
                    return true;
                }
                previous = current;
                current = current.Next;
            } while (current != FirstNode);

            return false;
        }

        public bool IsEmpty
        {
            get { return CountNode == 0; }
        }

        public void Sort()
        {
            Node<T> buf, current, previous = null;
            int score;
            bool toggle;
            do
            {
                score = 0;
                toggle = false;
                for (int i = 0; i < CountNode - 1; i++)
                {
                    current = FirstNode;
                    if (i == 0 && current.Data.CompareTo(current.Next.Data) > 0)
                    {
                        toggle = true;
                        buf = current.Next;
                        current.Next = current.Next.Next;
                        buf.Next = current;
                        LastNode.Next = buf;
                        FirstNode = buf;
                    }
                    else
                    {
                        for (int j = 0; j < i; j++)
                        {
                            previous = current;
                            current = current.Next;
                        }
                        if (current.Data.CompareTo(current.Next.Data) > 0)
                        {
                            toggle = true;
                            buf = current.Next;
                            current.Next = current.Next.Next;
                            buf.Next = current;
                            previous.Next = buf;
                            if (i == CountNode - 2)
                                LastNode = buf.Next;
                        }
                    }
                    score++;
                }

            } while (score == CountNode - 1 && toggle);
        }

        public int BinarySearch(T Data)
        {
            Node<T> current;
            int leftPosition = 0;
            int rightPosition = CountNode - 1;
            int searchPosition = -1;
            while (leftPosition <= rightPosition)
            {
                int middlePosition = (rightPosition + leftPosition) / 2;
                current = FirstNode;
                for (int j = 0; j < middlePosition; j++)
                    current = current.Next;
                if (current.Data.CompareTo(Data) == 0)
                {
                    searchPosition = middlePosition;
                    break;
                }
                if (current.Data.CompareTo(Data) < 0)
                    leftPosition = middlePosition + 1;
                else
                    rightPosition = middlePosition - 1;

            }
            return searchPosition;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)this).GetEnumerator();
        }
        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            Node<T> current = FirstNode;
            do
            {
                if (current != null)
                {
                    yield return current.Data;
                    current = current.Next;
                }
            }
            while (current != FirstNode);
        }
    }
}
