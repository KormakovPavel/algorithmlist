﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AlgorithmList
{
    interface IAlgorithm<T>
    {
        void Sort();
        int BinarySearch(T Data);
    }

}
