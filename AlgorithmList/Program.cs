﻿using System;

namespace AlgorithmList
{
    class Program
    {
        static void Main()
        {
            int position;
            CircularLinkedList<int> circularListInt = new CircularLinkedList<int>() { 90, 48, 36, 11, 64, 53, 80, 7, 12, 67, 1 };
            Console.WriteLine("\nИсходный список чисел:");
            foreach (var item in circularListInt)
                Console.WriteLine(item);

            circularListInt.Sort();
            Console.WriteLine("\nОтсортированный список чисел:");
            foreach (var item in circularListInt)
                Console.WriteLine(item);

            Console.Write("\nВведите элемент из списка: ");
            Int32.TryParse(Console.ReadLine(), out int ElementSearchInt);
            if (ElementSearchInt != 0)
            {
                position = circularListInt.BinarySearch(ElementSearchInt);
                if (position != -1)
                    Console.WriteLine($"Позиция найденого элемента в списке: {position}");
                else
                    Console.WriteLine("Элемент не найден");
            }
            else
                Console.WriteLine("Элемент не найден");

            CircularLinkedList<string> circularListString = new CircularLinkedList<string>() { "Андрей", "Ирина", "Максим", "Егор", "Дмитрий" };
            Console.WriteLine("\nИсходный список имен:");
            foreach (var item in circularListString)
                Console.WriteLine(item);

            circularListString.Sort();
            Console.WriteLine("\nОтсортированный список имен:");
            foreach (var item in circularListString)
                Console.WriteLine(item);

            Console.Write("\nВведите имя из списка: ");
            string ElementSearch = Console.ReadLine();
            position = circularListString.BinarySearch(ElementSearch);
            if (position != -1)
                Console.WriteLine($"Позиция найденого имени в списке: {position}");
            else
                Console.WriteLine("Имя не найдено");

            Console.ReadLine();
        }
    }
}
